<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dev2' );

/** MySQL database username */
define( 'DB_USER', 'dev2-user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'YaDTFkNx' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3308' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mA^PkD#^).gPR_qk7d_qqBFPk#GXJ+ht_}qG0??P)oe+xYu<_]@M);L`Ki-FV!@e' );
define( 'SECURE_AUTH_KEY',  'pK4^Jj}xQEs(xD~~:ov:P&0@$Dw,XE}NlebY1=Jq`.QVhUmy~e(ZISUpMMg8_FYj' );
define( 'LOGGED_IN_KEY',    'C:|f4WKbFg6=!q<u b{]&wbJc`Ojc9O3Z%]/<H&%vH8#N+I4cz|;_0* QJ}xS)Ae' );
define( 'NONCE_KEY',        'MX%Lw+{V~>.C1(#zfb1Vx`a+qcZ6bp*|2oqMY/@v)57!mR/Z~`eR(kj#!@2MYl8c' );
define( 'AUTH_SALT',        '^_{zm3Sn7368ah&k=3RF8X=1iqre?M/P`%8WlK>04}$.,z:b*>*[;ICHoQf^g.FM' );
define( 'SECURE_AUTH_SALT', 'v 3lBX4oPBh+8C?op}P$i{y$OOKk<_Dgh^IO83FYFfk~3qX[3|2hm>Cn;2]$pb$;' );
define( 'LOGGED_IN_SALT',   'RbJ:>qAV7kj;}bWly#cuw.u<s0c-FUqN:CLV%kSC8Eaz3$!_UXo]|(8G)lz&V7Lh' );
define( 'NONCE_SALT',       'hP;_=AQ$vI8`!DB5xie]ICqI}Y?CJ|DXdG$6z_ 4;Dxy7o06;!i)}Bo(($_3tRiB' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
