-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mar. 24 déc. 2019 à 10:25
-- Version du serveur :  10.1.34-MariaDB
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `jt`
--

-- --------------------------------------------------------

--
-- Structure de la table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-09-27 13:44:12', '2019-09-27 13:44:12', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://laroujou3.com', 'yes'),
(2, 'home', 'http://laroujou3.com', 'yes'),
(3, 'blogname', 'Justice Transitionnelle', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'elmanaahoussem@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:106:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:37:\"documents/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"documents/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"documents/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"documents/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"documents/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"documents/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"documents/([^/]+)/embed/?$\";s:42:\"index.php?documents=$matches[1]&embed=true\";s:30:\"documents/([^/]+)/trackback/?$\";s:36:\"index.php?documents=$matches[1]&tb=1\";s:38:\"documents/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?documents=$matches[1]&paged=$matches[2]\";s:45:\"documents/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?documents=$matches[1]&cpage=$matches[2]\";s:34:\"documents/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?documents=$matches[1]&page=$matches[2]\";s:26:\"documents/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:36:\"documents/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:56:\"documents/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"documents/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:51:\"documents/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:32:\"documents/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:30:\"advanced-custom-fields/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'jt', 'yes'),
(41, 'stylesheet', 'jt', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:6:{i:1577180653;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1577195052;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1577195053;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1577195067;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1577195068;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(117, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1569592106;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(120, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1577179177;s:7:\"checked\";a:1:{s:2:\"jt\";s:9:\"0.1 alpha\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(128, 'can_compress_scripts', '1', 'no'),
(139, 'current_theme', 'ShababLive Theme', 'yes'),
(140, 'theme_mods_jt', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:5:{s:6:\"arabic\";i:2;s:27:\"footer arabic partners menu\";i:3;s:24:\"footer arabic links menu\";i:4;s:18:\"arabic mobile menu\";i:5;s:11:\"arabic menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(142, 'recovery_mode_email_last_sent', '1575462214', 'yes'),
(167, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:3:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.3.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.3.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.3.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.3.2\";s:7:\"version\";s:5:\"5.3.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";s:9:\"new_files\";s:1:\"1\";}i:2;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.5.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.5.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.5-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.5-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.5-partial-4.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.5-rollback-4.zip\";}s:7:\"current\";s:5:\"5.2.5\";s:7:\"version\";s:5:\"5.2.5\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:5:\"5.2.4\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1577179166;s:15:\"version_checked\";s:5:\"5.2.4\";s:12:\"translations\";a:0:{}}', 'no'),
(169, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:24:\"elmanaahoussem@gmail.com\";s:7:\"version\";s:5:\"5.2.4\";s:9:\"timestamp\";i:1572958886;}', 'no'),
(213, 'recently_activated', 'a:0:{}', 'yes'),
(214, 'acf_version', '5.8.7', 'yes'),
(218, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(381, '_site_transient_timeout_browser_237aa6249591b6a7ad6962bc73492c77', '1577704626', 'no'),
(382, '_site_transient_browser_237aa6249591b6a7ad6962bc73492c77', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"79.0.3945.88\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(383, '_site_transient_timeout_php_check_70b0f2e71e10d268b0bc7f081eb76a94', '1577704627', 'no'),
(384, '_site_transient_php_check_70b0f2e71e10d268b0bc7f081eb76a94', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(387, '_site_transient_timeout_theme_roots', '1577180976', 'no'),
(388, '_site_transient_theme_roots', 'a:1:{s:2:\"jt\";s:7:\"/themes\";}', 'no'),
(389, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1577179179;s:7:\"checked\";a:3:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.7\";s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:9:\"hello.php\";s:5:\"1.7.2\";}s:8:\"response\";a:1:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.7\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Structure de la table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(10, 8, '_edit_lock', '1569593937:1'),
(11, 8, '_wp_page_template', 'contact.php'),
(12, 10, '_edit_lock', '1569593958:1'),
(13, 10, '_wp_page_template', 'political-parties.php'),
(14, 12, '_edit_lock', '1569594025:1'),
(15, 12, '_wp_page_template', 'comingsoon.php'),
(16, 14, '_edit_lock', '1572960320:1'),
(17, 14, '_wp_page_template', 'about-us.php'),
(19, 17, '_edit_lock', '1569594209:1'),
(20, 17, '_wp_page_template', 'sondage.php'),
(24, 23, '_edit_lock', '1574776459:1'),
(25, 23, '_wp_page_template', 'default'),
(26, 26, '_edit_last', '1'),
(27, 26, '_edit_lock', '1574767812:1'),
(28, 27, '_edit_last', '1'),
(29, 27, '_edit_lock', '1575989135:1'),
(30, 30, '_edit_lock', '1574771147:1'),
(31, 30, '_wp_page_template', 'documentation.php'),
(32, 33, '_edit_last', '1'),
(33, 33, '_edit_lock', '1574785223:1'),
(34, 33, 'type', 's1'),
(35, 33, '_type', 'field_5ddd19cf113ef'),
(36, 33, 'date', '20191106'),
(37, 33, '_date', 'field_5ddd19fe866bd'),
(38, 34, '_edit_last', '1'),
(39, 34, '_edit_lock', '1574785224:1'),
(40, 34, 'type', 's2'),
(41, 34, '_type', 'field_5ddd19cf113ef'),
(42, 34, 'date', '20191115'),
(43, 34, '_date', 'field_5ddd19fe866bd'),
(44, 35, '_edit_last', '1'),
(45, 35, '_edit_lock', '1575991397:1'),
(46, 35, 'type', 's3'),
(47, 35, '_type', 'field_5ddd19cf113ef'),
(48, 35, 'date', '20190516'),
(49, 35, '_date', 'field_5ddd19fe866bd'),
(50, 36, '_edit_last', '1'),
(51, 36, '_edit_lock', '1575991298:1'),
(52, 36, 'type', 's3'),
(53, 36, '_type', 'field_5ddd19cf113ef'),
(54, 36, 'date', '20170223'),
(55, 36, '_date', 'field_5ddd19fe866bd'),
(56, 37, '_menu_item_type', 'post_type'),
(57, 37, '_menu_item_menu_item_parent', '0'),
(58, 37, '_menu_item_object_id', '30'),
(59, 37, '_menu_item_object', 'page'),
(60, 37, '_menu_item_target', ''),
(61, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(62, 37, '_menu_item_xfn', ''),
(63, 37, '_menu_item_url', ''),
(65, 38, '_menu_item_type', 'post_type'),
(66, 38, '_menu_item_menu_item_parent', '0'),
(67, 38, '_menu_item_object_id', '17'),
(68, 38, '_menu_item_object', 'page'),
(69, 38, '_menu_item_target', ''),
(70, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(71, 38, '_menu_item_xfn', ''),
(72, 38, '_menu_item_url', ''),
(74, 39, '_menu_item_type', 'post_type'),
(75, 39, '_menu_item_menu_item_parent', '0'),
(76, 39, '_menu_item_object_id', '10'),
(77, 39, '_menu_item_object', 'page'),
(78, 39, '_menu_item_target', ''),
(79, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(80, 39, '_menu_item_xfn', ''),
(81, 39, '_menu_item_url', ''),
(83, 40, '_menu_item_type', 'post_type'),
(84, 40, '_menu_item_menu_item_parent', '0'),
(85, 40, '_menu_item_object_id', '8'),
(86, 40, '_menu_item_object', 'page'),
(87, 40, '_menu_item_target', ''),
(88, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(89, 40, '_menu_item_xfn', ''),
(90, 40, '_menu_item_url', ''),
(92, 41, '_menu_item_type', 'post_type'),
(93, 41, '_menu_item_menu_item_parent', '0'),
(94, 41, '_menu_item_object_id', '30'),
(95, 41, '_menu_item_object', 'page'),
(96, 41, '_menu_item_target', ''),
(97, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(98, 41, '_menu_item_xfn', ''),
(99, 41, '_menu_item_url', ''),
(100, 41, '_menu_item_orphaned', '1574773059'),
(101, 42, '_menu_item_type', 'post_type'),
(102, 42, '_menu_item_menu_item_parent', '0'),
(103, 42, '_menu_item_object_id', '17'),
(104, 42, '_menu_item_object', 'page'),
(105, 42, '_menu_item_target', ''),
(106, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(107, 42, '_menu_item_xfn', ''),
(108, 42, '_menu_item_url', ''),
(109, 42, '_menu_item_orphaned', '1574773060'),
(110, 43, '_menu_item_type', 'post_type'),
(111, 43, '_menu_item_menu_item_parent', '0'),
(112, 43, '_menu_item_object_id', '10'),
(113, 43, '_menu_item_object', 'page'),
(114, 43, '_menu_item_target', ''),
(115, 43, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(116, 43, '_menu_item_xfn', ''),
(117, 43, '_menu_item_url', ''),
(118, 43, '_menu_item_orphaned', '1574773061'),
(119, 44, '_menu_item_type', 'post_type'),
(120, 44, '_menu_item_menu_item_parent', '0'),
(121, 44, '_menu_item_object_id', '8'),
(122, 44, '_menu_item_object', 'page'),
(123, 44, '_menu_item_target', ''),
(124, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(125, 44, '_menu_item_xfn', ''),
(126, 44, '_menu_item_url', ''),
(127, 44, '_menu_item_orphaned', '1574773062'),
(128, 45, '_menu_item_type', 'post_type'),
(129, 45, '_menu_item_menu_item_parent', '0'),
(130, 45, '_menu_item_object_id', '30'),
(131, 45, '_menu_item_object', 'page'),
(132, 45, '_menu_item_target', ''),
(133, 45, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(134, 45, '_menu_item_xfn', ''),
(135, 45, '_menu_item_url', ''),
(137, 46, '_menu_item_type', 'post_type'),
(138, 46, '_menu_item_menu_item_parent', '0'),
(139, 46, '_menu_item_object_id', '17'),
(140, 46, '_menu_item_object', 'page'),
(141, 46, '_menu_item_target', ''),
(142, 46, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(143, 46, '_menu_item_xfn', ''),
(144, 46, '_menu_item_url', ''),
(146, 47, '_menu_item_type', 'post_type'),
(147, 47, '_menu_item_menu_item_parent', '0'),
(148, 47, '_menu_item_object_id', '10'),
(149, 47, '_menu_item_object', 'page'),
(150, 47, '_menu_item_target', ''),
(151, 47, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(152, 47, '_menu_item_xfn', ''),
(153, 47, '_menu_item_url', ''),
(155, 48, '_menu_item_type', 'post_type'),
(156, 48, '_menu_item_menu_item_parent', '0'),
(157, 48, '_menu_item_object_id', '8'),
(158, 48, '_menu_item_object', 'page'),
(159, 48, '_menu_item_target', ''),
(160, 48, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(161, 48, '_menu_item_xfn', ''),
(162, 48, '_menu_item_url', ''),
(164, 49, '_edit_lock', '1575459849:1'),
(167, 51, '_edit_last', '1'),
(168, 51, '_edit_lock', '1574773579:1'),
(169, 51, '_wp_trash_meta_status', 'publish'),
(170, 51, '_wp_trash_meta_time', '1574773727'),
(171, 51, '_wp_desired_post_slug', 'group_5ddd23a809bd5'),
(172, 52, '_wp_trash_meta_status', 'publish'),
(173, 52, '_wp_trash_meta_time', '1574773727'),
(174, 52, '_wp_desired_post_slug', 'field_5ddd23ba0811f'),
(176, 33, 'file', '68'),
(177, 33, '_file', 'field_5ddd23e5a63f3'),
(178, 56, '_menu_item_type', 'custom'),
(179, 56, '_menu_item_menu_item_parent', '0'),
(180, 56, '_menu_item_object_id', '56'),
(181, 56, '_menu_item_object', 'custom'),
(182, 56, '_menu_item_target', ''),
(183, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(184, 56, '_menu_item_xfn', ''),
(185, 56, '_menu_item_url', 'http://www.albawsala.com'),
(187, 57, '_menu_item_type', 'post_type'),
(188, 57, '_menu_item_menu_item_parent', '0'),
(189, 57, '_menu_item_object_id', '30'),
(190, 57, '_menu_item_object', 'page'),
(191, 57, '_menu_item_target', ''),
(192, 57, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(193, 57, '_menu_item_xfn', ''),
(194, 57, '_menu_item_url', ''),
(196, 58, '_menu_item_type', 'post_type'),
(197, 58, '_menu_item_menu_item_parent', '0'),
(198, 58, '_menu_item_object_id', '17'),
(199, 58, '_menu_item_object', 'page'),
(200, 58, '_menu_item_target', ''),
(201, 58, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(202, 58, '_menu_item_xfn', ''),
(203, 58, '_menu_item_url', ''),
(205, 59, '_menu_item_type', 'post_type'),
(206, 59, '_menu_item_menu_item_parent', '0'),
(207, 59, '_menu_item_object_id', '10'),
(208, 59, '_menu_item_object', 'page'),
(209, 59, '_menu_item_target', ''),
(210, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(211, 59, '_menu_item_xfn', ''),
(212, 59, '_menu_item_url', ''),
(214, 60, '_menu_item_type', 'post_type'),
(215, 60, '_menu_item_menu_item_parent', '0'),
(216, 60, '_menu_item_object_id', '8'),
(217, 60, '_menu_item_object', 'page'),
(218, 60, '_menu_item_target', ''),
(219, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(220, 60, '_menu_item_xfn', ''),
(221, 60, '_menu_item_url', ''),
(223, 61, '_edit_lock', '1574779919:1'),
(224, 61, '_wp_page_template', 'single-documents.php'),
(228, 36, 'file', '68'),
(229, 36, '_file', 'field_5ddd23e5a63f3'),
(231, 34, 'file', '68'),
(232, 34, '_file', 'field_5ddd23e5a63f3'),
(233, 35, 'file', '68'),
(234, 35, '_file', 'field_5ddd23e5a63f3'),
(235, 69, '_menu_item_type', 'custom'),
(236, 69, '_menu_item_menu_item_parent', '0'),
(237, 69, '_menu_item_object_id', '69'),
(238, 69, '_menu_item_object', 'custom'),
(239, 69, '_menu_item_target', ''),
(240, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(241, 69, '_menu_item_xfn', ''),
(242, 69, '_menu_item_url', 'https://ftdes.net/ar/'),
(244, 70, '_menu_item_type', 'custom'),
(245, 70, '_menu_item_menu_item_parent', '0'),
(246, 70, '_menu_item_object_id', '70'),
(247, 70, '_menu_item_object', 'custom'),
(248, 70, '_menu_item_target', ''),
(249, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(250, 70, '_menu_item_xfn', ''),
(251, 70, '_menu_item_url', 'https://www.asf.be/action/field-offices/asf-in-tunisia/'),
(252, 36, 'filee_text', 'aaaaa'),
(253, 36, '_filee_text', 'field_5de67ecfc7616'),
(254, 36, 'filee_fichier', '68'),
(255, 36, '_filee_fichier', 'field_5de67ed8c7617'),
(256, 36, 'filee', ''),
(257, 36, '_filee', 'field_5de67ebfc7615'),
(258, 36, 'filee_copy_', 'bbbbb'),
(259, 36, '_filee_copy_', 'field_5de67ef318baf'),
(260, 36, 'filee_copy_fichier', '68'),
(261, 36, '_filee_copy_fichier', 'field_5de67ef318bb0'),
(262, 36, 'filee_copy', ''),
(263, 36, '_filee_copy', 'field_5de67ef318bae'),
(264, 81, '_edit_last', '1'),
(265, 81, '_edit_lock', '1575455247:1'),
(266, 84, '_edit_last', '1'),
(267, 84, '_edit_lock', '1577179396:1'),
(268, 84, 'introduction', 'la première'),
(269, 84, '_introduction', 'field_5de7815266751'),
(270, 84, 'video', 'http://laroujou3.com/wp-content/themes/jt/assets/video/adela.mp4'),
(271, 84, '_video', 'field_5de797fbe556c'),
(272, 81, '_wp_trash_meta_status', 'publish'),
(273, 81, '_wp_trash_meta_time', '1575457258'),
(274, 81, '_wp_desired_post_slug', 'group_5de7814602103'),
(275, 82, '_wp_trash_meta_status', 'publish'),
(276, 82, '_wp_trash_meta_time', '1575457259'),
(277, 82, '_wp_desired_post_slug', 'field_5de7815266751'),
(278, 83, '_wp_trash_meta_status', 'publish'),
(279, 83, '_wp_trash_meta_time', '1575457260'),
(280, 83, '_wp_desired_post_slug', 'field_5de7816366752'),
(281, 85, '_wp_trash_meta_status', 'publish'),
(282, 85, '_wp_trash_meta_time', '1575457260'),
(283, 85, '_wp_desired_post_slug', 'field_5de78a7917e14'),
(284, 88, '_edit_last', '1'),
(285, 88, '_edit_lock', '1576059859:1'),
(288, 99, '_edit_last', '1'),
(289, 99, '_edit_lock', '1575464289:1'),
(290, 99, 'type', 's1'),
(291, 99, '_type', 'field_5ddd19cf113ef'),
(292, 99, 'date', ''),
(293, 99, '_date', 'field_5ddd19fe866bd'),
(294, 99, 'file', ''),
(295, 99, '_file', 'field_5ddd23e5a63f3'),
(296, 99, 'filee_text', ''),
(297, 99, '_filee_text', 'field_5de67ecfc7616'),
(298, 99, 'filee_fichier', ''),
(299, 99, '_filee_fichier', 'field_5de67ed8c7617'),
(300, 99, 'filee', ''),
(301, 99, '_filee', 'field_5de67ebfc7615'),
(302, 99, 'filee_copy_', ''),
(303, 99, '_filee_copy_', 'field_5de67ef318baf'),
(304, 99, 'filee_copy_fichier', ''),
(305, 99, '_filee_copy_fichier', 'field_5de67ef318bb0'),
(306, 99, 'filee_copy', ''),
(307, 99, '_filee_copy', 'field_5de67ef318bae'),
(308, 99, 'customdata_group', 'a:1:{i:0;a:2:{s:9:\"TitleItem\";s:3:\"aaa\";s:16:\"TitleDescription\";s:7:\"  aaaaa\";}}'),
(309, 99, '_wp_trash_meta_status', 'draft'),
(310, 99, '_wp_trash_meta_time', '1575464439'),
(311, 99, '_wp_desired_post_slug', ''),
(314, 100, '_edit_last', '1'),
(315, 100, 'type', 's2'),
(316, 100, '_type', 'field_5ddd19cf113ef'),
(317, 100, 'date', '20190712'),
(318, 100, '_date', 'field_5ddd19fe866bd'),
(319, 100, 'file', '68'),
(320, 100, '_file', 'field_5ddd23e5a63f3'),
(321, 100, 'filee_text', ''),
(322, 100, '_filee_text', 'field_5de67ecfc7616'),
(323, 100, 'filee_fichier', ''),
(324, 100, '_filee_fichier', 'field_5de67ed8c7617'),
(325, 100, 'filee', ''),
(326, 100, '_filee', 'field_5de67ebfc7615'),
(327, 100, 'filee_copy_', ''),
(328, 100, '_filee_copy_', 'field_5de67ef318baf'),
(329, 100, 'filee_copy_fichier', ''),
(330, 100, '_filee_copy_fichier', 'field_5de67ef318bb0'),
(331, 100, 'filee_copy', ''),
(332, 100, '_filee_copy', 'field_5de67ef318bae'),
(333, 100, 'customdata_group', 'a:2:{i:0;a:2:{s:9:\"TitleItem\";s:16:\"002-document.svg\";s:16:\"TitleDescription\";s:2:\"  \";}i:1;a:2:{s:9:\"TitleItem\";s:19:\"009-legal-paper.svg\";s:16:\"TitleDescription\";s:0:\"\";}}'),
(334, 100, '_edit_lock', '1575991284:1'),
(338, 100, 'files', '112,113,110,121'),
(339, 100, '_files', 'field_5ddd23e5a63f3'),
(346, 34, '_wp_trash_meta_status', 'publish'),
(347, 34, '_wp_trash_meta_time', '1575986193'),
(348, 34, '_wp_desired_post_slug', '2%d9%88%d8%ab%d9%8a%d9%82%d8%a9'),
(349, 33, '_wp_trash_meta_status', 'publish'),
(350, 33, '_wp_trash_meta_time', '1575986197'),
(351, 33, '_wp_desired_post_slug', '1%d9%88%d8%ab%d9%8a%d9%82%d8%a9'),
(352, 107, '_edit_last', '1'),
(353, 107, '_edit_lock', '1575991270:1'),
(360, 107, 'type', 's1'),
(361, 107, '_type', 'field_5ddd19cf113ef'),
(362, 107, 'date', '20191209'),
(363, 107, '_date', 'field_5ddd19fe866bd'),
(364, 107, 'files', ',112,111,120,119'),
(365, 107, '_files', 'field_5ddd23e5a63f3'),
(366, 35, '_wp_old_slug', '3%d9%88%d8%ab%d9%8a%d9%82%d8%a9__trashed'),
(367, 36, '_wp_old_slug', '4%d9%88%d8%ab%d9%8a%d9%82%d8%a9__trashed'),
(368, 35, 'files', ',113,110,111,112,122,121,120'),
(369, 35, '_files', 'field_5ddd23e5a63f3'),
(370, 36, 'files', ',112,120,121,119'),
(371, 36, '_files', 'field_5ddd23e5a63f3'),
(372, 116, '_edit_last', '1'),
(373, 116, '_edit_lock', '1575991251:1'),
(374, 116, 'type', 's1'),
(375, 116, '_type', 'field_5ddd19cf113ef'),
(376, 116, 'date', '20191202'),
(377, 116, '_date', 'field_5ddd19fe866bd'),
(378, 116, 'files', ',113,112,122,120'),
(379, 116, '_files', 'field_5ddd23e5a63f3'),
(380, 117, '_edit_last', '1'),
(381, 117, 'type', 's2'),
(382, 117, '_type', 'field_5ddd19cf113ef'),
(383, 117, 'date', '20190808'),
(384, 117, '_date', 'field_5ddd19fe866bd'),
(385, 117, 'files', ',122,121,120,119'),
(386, 117, '_files', 'field_5ddd23e5a63f3'),
(387, 117, '_edit_lock', '1575991233:1'),
(388, 119, '_wp_attached_file', '2019/12/وثيقة-4.txt'),
(389, 120, '_wp_attached_file', '2019/12/وثيقة-1.txt'),
(390, 121, '_wp_attached_file', '2019/12/وثيقة-2.txt'),
(391, 122, '_wp_attached_file', '2019/12/وثيقة-3.txt'),
(392, 130, '_edit_last', '1'),
(393, 130, '_edit_lock', '1577179278:1'),
(394, 130, 'video', 'http://laroujou3.com/wp-content/themes/jt/assets/video/adela.mp4'),
(395, 130, '_video', 'field_5de797fbe556c');

-- --------------------------------------------------------

--
-- Structure de la table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-09-27 13:44:12', '2019-09-27 13:44:12', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2019-09-27 13:44:12', '2019-09-27 13:44:12', '', 0, 'http://laroujou3.com/?p=1', 0, 'post', '', 1),
(8, 1, '2019-09-27 14:21:18', '2019-09-27 14:21:18', '', 'contact us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2019-09-27 14:21:18', '2019-09-27 14:21:18', '', 0, 'http://laroujou3.com/?page_id=8', 0, 'page', '', 0),
(9, 1, '2019-09-27 14:21:18', '2019-09-27 14:21:18', '', 'contact us', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-09-27 14:21:18', '2019-09-27 14:21:18', '', 8, 'http://laroujou3.com/2019/09/27/8-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-09-27 14:21:42', '2019-09-27 14:21:42', '', 'political parties', '', 'publish', 'closed', 'closed', '', 'political-parties', '', '', '2019-09-27 14:21:42', '2019-09-27 14:21:42', '', 0, 'http://laroujou3.com/?page_id=10', 0, 'page', '', 0),
(11, 1, '2019-09-27 14:21:42', '2019-09-27 14:21:42', '', 'political parties', '', 'inherit', 'closed', 'closed', '', '10-revision-v1', '', '', '2019-09-27 14:21:42', '2019-09-27 14:21:42', '', 10, 'http://laroujou3.com/2019/09/27/10-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2019-09-27 14:22:05', '2019-09-27 14:22:05', '', 'coming soon', '', 'publish', 'closed', 'closed', '', 'coming-soon', '', '', '2019-09-27 14:22:05', '2019-09-27 14:22:05', '', 0, 'http://laroujou3.com/?page_id=12', 0, 'page', '', 0),
(13, 1, '2019-09-27 14:22:05', '2019-09-27 14:22:05', '', 'coming soon', '', 'inherit', 'closed', 'closed', '', '12-revision-v1', '', '', '2019-09-27 14:22:05', '2019-09-27 14:22:05', '', 12, 'http://laroujou3.com/2019/09/27/12-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-09-27 14:22:57', '2019-09-27 14:22:57', '', 'about us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2019-11-05 13:27:07', '2019-11-05 13:27:07', '', 0, 'http://laroujou3.com/?page_id=14', 0, 'page', '', 0),
(15, 1, '2019-09-27 14:22:57', '2019-09-27 14:22:57', '', 'about us', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-09-27 14:22:57', '2019-09-27 14:22:57', '', 14, 'http://laroujou3.com/2019/09/27/14-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2019-09-27 14:23:35', '2019-09-27 14:23:35', '', 'sondage', '', 'publish', 'closed', 'closed', '', 'sondage', '', '', '2019-09-27 14:23:35', '2019-09-27 14:23:35', '', 0, 'http://laroujou3.com/?page_id=17', 0, 'page', '', 0),
(18, 1, '2019-09-27 14:23:35', '2019-09-27 14:23:35', '', 'sondage', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2019-09-27 14:23:35', '2019-09-27 14:23:35', '', 17, 'http://laroujou3.com/2019/09/27/17-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2019-11-05 13:30:02', '2019-11-05 13:30:02', '<!-- wp:paragraph -->\n<p>uaeigoae aegoeagpaoe eiaopgjapaeogae </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>eagaeigaegaegaeiueae</p>\n<!-- /wp:paragraph -->', 'Title news', '', 'publish', 'closed', 'closed', '', 'title-news', '', '', '2019-11-26 13:56:33', '2019-11-26 13:56:33', '', 0, 'http://laroujou3.com/?page_id=23', 0, 'page', '', 0),
(24, 1, '2019-11-05 13:30:02', '2019-11-05 13:30:02', '<!-- wp:paragraph -->\n<p>uaeigoae aegoeagpaoe eiaopgjapaeogae </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>eagaeigaegaegaeiueae</p>\n<!-- /wp:paragraph -->', 'Title news', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2019-11-05 13:30:02', '2019-11-05 13:30:02', '', 23, 'http://laroujou3.com/2019/11/05/23-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2019-11-26 11:30:11', '0000-00-00 00:00:00', '', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-11-26 11:30:11', '2019-11-26 11:30:11', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=26', 0, 'documents', '', 0),
(27, 1, '2019-11-26 12:26:34', '2019-11-26 12:26:34', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:9:\"documents\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'documents field', 'documents-field', 'publish', 'closed', 'closed', '', 'group_5ddd1999b62cd', '', '', '2019-12-10 13:56:06', '2019-12-10 13:56:06', '', 0, 'http://laroujou3.com/?post_type=acf-field-group&#038;p=27', 0, 'acf-field-group', '', 0),
(28, 1, '2019-11-26 12:26:34', '2019-11-26 12:26:34', 'a:13:{s:4:\"type\";s:6:\"select\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:7:\"choices\";a:4:{s:2:\"s1\";s:2:\"s1\";s:2:\"s2\";s:2:\"s2\";s:2:\"s3\";s:2:\"s3\";s:2:\"s4\";s:2:\"s4\";}s:13:\"default_value\";a:0:{}s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:2:\"ui\";i:0;s:13:\"return_format\";s:5:\"value\";s:4:\"ajax\";i:0;s:11:\"placeholder\";s:0:\"\";}', 'type', 'type', 'publish', 'closed', 'closed', '', 'field_5ddd19cf113ef', '', '', '2019-11-26 12:26:34', '2019-11-26 12:26:34', '', 27, 'http://laroujou3.com/?post_type=acf-field&p=28', 0, 'acf-field', '', 0),
(29, 1, '2019-11-26 12:27:08', '2019-11-26 12:27:08', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"Y.m.d\";s:13:\"return_format\";s:5:\"Y.m.d\";s:9:\"first_day\";i:1;}', 'date', 'date', 'publish', 'closed', 'closed', '', 'field_5ddd19fe866bd', '', '', '2019-11-26 12:42:03', '2019-11-26 12:42:03', '', 27, 'http://laroujou3.com/?post_type=acf-field&#038;p=29', 1, 'acf-field', '', 0),
(30, 1, '2019-11-26 12:28:07', '2019-11-26 12:28:07', '', 'documentation', '', 'publish', 'closed', 'closed', '', 'documentation', '', '', '2019-11-26 12:28:07', '2019-11-26 12:28:07', '', 0, 'http://laroujou3.com/?page_id=30', 0, 'page', '', 0),
(31, 1, '2019-11-26 12:28:07', '2019-11-26 12:28:07', '', 'documentation', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2019-11-26 12:28:07', '2019-11-26 12:28:07', '', 30, 'http://laroujou3.com/2019/11/26/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2019-11-26 12:34:07', '2019-11-26 12:34:07', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '1وثيقة', '', 'trash', 'closed', 'closed', '', '1%d9%88%d8%ab%d9%8a%d9%82%d8%a9__trashed', '', '', '2019-12-10 13:56:37', '2019-12-10 13:56:37', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=33', 0, 'documents', '', 0),
(34, 1, '2019-11-26 12:35:14', '2019-11-26 12:35:14', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '2وثيقة', '', 'trash', 'closed', 'closed', '', '2%d9%88%d8%ab%d9%8a%d9%82%d8%a9__trashed', '', '', '2019-12-10 13:56:33', '2019-12-10 13:56:33', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=34', 0, 'documents', '', 0),
(35, 1, '2019-11-26 12:35:53', '2019-11-26 12:35:53', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '3وثيقة', '', 'publish', 'closed', 'closed', '', '3%d9%88%d8%ab%d9%8a%d9%82%d8%a9', '', '', '2019-12-10 15:24:16', '2019-12-10 15:24:16', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=35', 0, 'documents', '', 0),
(36, 1, '2019-11-26 12:37:06', '2019-11-26 12:37:06', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '4 وثيقة', '', 'publish', 'closed', 'closed', '', '4%d9%88%d8%ab%d9%8a%d9%82%d8%a9', '', '', '2019-12-10 15:24:00', '2019-12-10 15:24:00', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=36', 0, 'documents', '', 0),
(37, 1, '2019-11-26 12:51:13', '2019-11-26 12:51:13', '', 'توثيق', '', 'publish', 'closed', 'closed', '', '%d8%aa%d9%88%d8%ab%d9%8a%d9%82', '', '', '2019-11-26 15:08:25', '2019-11-26 15:08:25', '', 0, 'http://laroujou3.com/?p=37', 1, 'nav_menu_item', '', 0),
(38, 1, '2019-11-26 12:51:13', '2019-11-26 12:51:13', '', 'سبر أراء', '', 'publish', 'closed', 'closed', '', '%d8%b3%d8%a8%d8%b1-%d8%a3%d8%b1%d8%a7%d8%a1', '', '', '2019-11-26 15:08:25', '2019-11-26 15:08:25', '', 0, 'http://laroujou3.com/?p=38', 4, 'nav_menu_item', '', 0),
(39, 1, '2019-11-26 12:51:13', '2019-11-26 12:51:13', '', 'أحزاب سياسية', '', 'publish', 'closed', 'closed', '', '%d8%a3%d8%ad%d8%b2%d8%a7%d8%a8-%d8%b3%d9%8a%d8%a7%d8%b3%d9%8a%d8%a9', '', '', '2019-11-26 15:08:25', '2019-11-26 15:08:25', '', 0, 'http://laroujou3.com/?p=39', 2, 'nav_menu_item', '', 0),
(40, 1, '2019-11-26 12:51:13', '2019-11-26 12:51:13', '', 'اتصل بنا', '', 'publish', 'closed', 'closed', '', '%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7', '', '', '2019-11-26 15:08:25', '2019-11-26 15:08:25', '', 0, 'http://laroujou3.com/?p=40', 3, 'nav_menu_item', '', 0),
(41, 1, '2019-11-26 12:57:38', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-11-26 12:57:38', '0000-00-00 00:00:00', '', 0, 'http://laroujou3.com/?p=41', 1, 'nav_menu_item', '', 0),
(42, 1, '2019-11-26 12:57:39', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-11-26 12:57:39', '0000-00-00 00:00:00', '', 0, 'http://laroujou3.com/?p=42', 1, 'nav_menu_item', '', 0),
(43, 1, '2019-11-26 12:57:40', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-11-26 12:57:40', '0000-00-00 00:00:00', '', 0, 'http://laroujou3.com/?p=43', 1, 'nav_menu_item', '', 0),
(44, 1, '2019-11-26 12:57:41', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2019-11-26 12:57:41', '0000-00-00 00:00:00', '', 0, 'http://laroujou3.com/?p=44', 1, 'nav_menu_item', '', 0),
(45, 1, '2019-11-26 12:59:15', '2019-11-26 12:59:15', '', 'توثيق', '', 'publish', 'closed', 'closed', '', '45', '', '', '2019-11-26 15:08:44', '2019-11-26 15:08:44', '', 0, 'http://laroujou3.com/?p=45', 1, 'nav_menu_item', '', 0),
(46, 1, '2019-11-26 12:59:16', '2019-11-26 12:59:16', '', 'سبر أراء', '', 'publish', 'closed', 'closed', '', '46', '', '', '2019-11-26 15:08:44', '2019-11-26 15:08:44', '', 0, 'http://laroujou3.com/?p=46', 3, 'nav_menu_item', '', 0),
(47, 1, '2019-11-26 12:59:16', '2019-11-26 12:59:16', '', 'أحزاب سياسية', '', 'publish', 'closed', 'closed', '', '47', '', '', '2019-11-26 15:08:44', '2019-11-26 15:08:44', '', 0, 'http://laroujou3.com/?p=47', 2, 'nav_menu_item', '', 0),
(48, 1, '2019-11-26 12:59:17', '2019-11-26 12:59:17', '', 'اتصل بنا', '', 'publish', 'closed', 'closed', '', '48', '', '', '2019-11-26 15:08:45', '2019-11-26 15:08:45', '', 0, 'http://laroujou3.com/?p=48', 4, 'nav_menu_item', '', 0),
(49, 1, '2019-11-26 13:07:43', '2019-11-26 13:07:43', '<!-- wp:paragraph -->\n<p>sssssssssssss</p>\n<!-- /wp:paragraph -->', 'post 1', '', 'publish', 'open', 'open', '', 'post-1', '', '', '2019-11-26 13:07:43', '2019-11-26 13:07:43', '', 0, 'http://laroujou3.com/?p=49', 0, 'post', '', 0),
(50, 1, '2019-11-26 13:07:43', '2019-11-26 13:07:43', '<!-- wp:paragraph -->\n<p>sssssssssssss</p>\n<!-- /wp:paragraph -->', 'post 1', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2019-11-26 13:07:43', '2019-11-26 13:07:43', '', 49, 'http://laroujou3.com/2019/11/26/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2019-11-26 13:08:32', '2019-11-26 13:08:32', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'posts field', 'posts-field', 'trash', 'closed', 'closed', '', 'group_5ddd23a809bd5__trashed', '', '', '2019-11-26 13:08:47', '2019-11-26 13:08:47', '', 0, 'http://laroujou3.com/?post_type=acf-field-group&#038;p=51', 0, 'acf-field-group', '', 0),
(52, 1, '2019-11-26 13:08:33', '2019-11-26 13:08:33', 'a:10:{s:4:\"type\";s:4:\"file\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:7:\"library\";s:3:\"all\";s:8:\"min_size\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'file', 'file', 'trash', 'closed', 'closed', '', 'field_5ddd23ba0811f__trashed', '', '', '2019-11-26 13:08:47', '2019-11-26 13:08:47', '', 51, 'http://laroujou3.com/?post_type=acf-field&#038;p=52', 0, 'acf-field', '', 0),
(53, 1, '2019-11-26 13:09:30', '2019-11-26 13:09:30', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'files', 'files', 'publish', 'closed', 'closed', '', 'field_5ddd23e5a63f3', '', '', '2019-12-04 14:44:27', '2019-12-04 14:44:27', '', 27, 'http://laroujou3.com/?post_type=acf-field&#038;p=53', 2, 'acf-field', '', 0),
(56, 1, '2019-11-26 13:34:30', '2019-11-26 13:34:30', '', 'البوصلة', '', 'publish', 'closed', 'closed', '', '%d8%a7%d9%84%d8%a8%d9%88%d8%b5%d9%84%d8%a9', '', '', '2019-11-26 15:38:12', '2019-11-26 15:38:12', '', 0, 'http://laroujou3.com/?p=56', 1, 'nav_menu_item', '', 0),
(57, 1, '2019-11-26 13:37:55', '2019-11-26 13:37:55', '', 'توثيق', '', 'publish', 'closed', 'closed', '', '%d8%aa%d9%88%d8%ab%d9%8a%d9%82-2', '', '', '2019-11-26 13:47:59', '2019-11-26 13:47:59', '', 0, 'http://laroujou3.com/?p=57', 1, 'nav_menu_item', '', 0),
(58, 1, '2019-11-26 13:37:55', '2019-11-26 13:37:55', '', 'سبر أراء', '', 'publish', 'closed', 'closed', '', '%d8%b3%d8%a8%d8%b1-%d8%a3%d8%b1%d8%a7%d8%a1-2', '', '', '2019-11-26 13:47:59', '2019-11-26 13:47:59', '', 0, 'http://laroujou3.com/?p=58', 3, 'nav_menu_item', '', 0),
(59, 1, '2019-11-26 13:37:55', '2019-11-26 13:37:55', '', 'أحزاب سياسية', '', 'publish', 'closed', 'closed', '', '%d8%a3%d8%ad%d8%b2%d8%a7%d8%a8-%d8%b3%d9%8a%d8%a7%d8%b3%d9%8a%d8%a9-2', '', '', '2019-11-26 13:47:59', '2019-11-26 13:47:59', '', 0, 'http://laroujou3.com/?p=59', 2, 'nav_menu_item', '', 0),
(60, 1, '2019-11-26 13:37:56', '2019-11-26 13:37:56', '', 'اتصل بنا', '', 'publish', 'closed', 'closed', '', '%d8%a7%d8%aa%d8%b5%d9%84-%d8%a8%d9%86%d8%a7-2', '', '', '2019-11-26 13:47:59', '2019-11-26 13:47:59', '', 0, 'http://laroujou3.com/?p=60', 4, 'nav_menu_item', '', 0),
(61, 1, '2019-11-26 13:49:54', '2019-11-26 13:49:54', '', 'documents', '', 'publish', 'closed', 'closed', '', 'documents', '', '', '2019-11-26 14:38:10', '2019-11-26 14:38:10', '', 0, 'http://laroujou3.com/?page_id=61', 0, 'page', '', 0),
(62, 1, '2019-11-26 13:49:54', '2019-11-26 13:49:54', '', 'single docment', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-11-26 13:49:54', '2019-11-26 13:49:54', '', 61, 'http://laroujou3.com/2019/11/26/61-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2019-11-26 13:51:08', '2019-11-26 13:51:08', '', 'document', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-11-26 13:51:08', '2019-11-26 13:51:08', '', 61, 'http://laroujou3.com/2019/11/26/61-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2019-11-26 14:34:21', '2019-11-26 14:34:21', '', 'documents', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-11-26 14:34:21', '2019-11-26 14:34:21', '', 61, 'http://laroujou3.com/2019/11/26/61-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2019-11-26 14:40:14', '2019-11-26 14:40:14', '', 'documents', '', 'inherit', 'closed', 'closed', '', '61-autosave-v1', '', '', '2019-11-26 14:40:14', '2019-11-26 14:40:14', '', 61, 'http://laroujou3.com/2019/11/26/61-autosave-v1/', 0, 'revision', '', 0),
(69, 1, '2019-11-26 15:37:43', '2019-11-26 15:37:43', '', 'المنتدى التونسي للحقوق الإقتصادية و الإجتماعية', '', 'publish', 'closed', 'closed', '', '%d8%a7%d9%84%d9%85%d9%86%d8%aa%d8%af%d9%89-%d8%a7%d9%84%d8%aa%d9%88%d9%86%d8%b3%d9%8a-%d9%84%d9%84%d8%ad%d9%82%d9%88%d9%82-%d8%a7%d9%84%d8%a5%d9%82%d8%aa%d8%b5%d8%a7%d8%af%d9%8a%d8%a9-%d9%88-%d8%a7', '', '', '2019-11-26 15:38:12', '2019-11-26 15:38:12', '', 0, 'http://laroujou3.com/?p=69', 2, 'nav_menu_item', '', 0),
(70, 1, '2019-11-26 15:38:12', '2019-11-26 15:38:12', '', 'محامون بلا حدود - تونس', '', 'publish', 'closed', 'closed', '', '%d9%85%d8%ad%d8%a7%d9%85%d9%88%d9%86-%d8%a8%d9%84%d8%a7-%d8%ad%d8%af%d9%88%d8%af-%d8%aa%d9%88%d9%86%d8%b3', '', '', '2019-11-26 15:38:12', '2019-11-26 15:38:12', '', 0, 'http://laroujou3.com/?p=70', 3, 'nav_menu_item', '', 0),
(81, 1, '2019-12-04 09:50:08', '2019-12-04 09:50:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"videos\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'video_field', 'video_field', 'trash', 'closed', 'closed', '', 'group_5de7814602103__trashed', '', '', '2019-12-04 11:00:59', '2019-12-04 11:00:59', '', 0, 'http://laroujou3.com/?post_type=acf-field-group&#038;p=81', 0, 'acf-field-group', '', 0),
(82, 1, '2019-12-04 09:50:50', '2019-12-04 09:50:50', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'introduction', 'introduction', 'trash', 'closed', 'closed', '', 'field_5de7815266751__trashed', '', '', '2019-12-04 11:00:59', '2019-12-04 11:00:59', '', 81, 'http://laroujou3.com/?post_type=acf-field&#038;p=82', 0, 'acf-field', '', 0),
(83, 1, '2019-12-04 09:50:50', '2019-12-04 09:50:50', 'a:7:{s:4:\"type\";s:6:\"oembed\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:5:\"width\";s:0:\"\";s:6:\"height\";s:0:\"\";}', 'video', 'video', 'trash', 'closed', 'closed', '', 'field_5de7816366752__trashed', '', '', '2019-12-04 11:01:00', '2019-12-04 11:01:00', '', 81, 'http://laroujou3.com/?post_type=acf-field&#038;p=83', 1, 'acf-field', '', 0),
(84, 1, '2019-12-04 09:59:48', '2019-12-04 09:59:48', 'la première', 'video1', '', 'publish', 'closed', 'closed', '', 'video1', '', '', '2019-12-24 09:23:16', '2019-12-24 09:23:16', '', 0, 'http://laroujou3.com/?post_type=videos&#038;p=84', 0, 'videos', '', 0),
(85, 1, '2019-12-04 10:29:44', '2019-12-04 10:29:44', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:3:\"mp4\";}', 'img', 'img', 'trash', 'closed', 'closed', '', 'field_5de78a7917e14__trashed', '', '', '2019-12-04 11:01:00', '2019-12-04 11:01:00', '', 81, 'http://laroujou3.com/?post_type=acf-field&#038;p=85', 2, 'acf-field', '', 0),
(88, 1, '2019-12-04 11:26:49', '2019-12-04 11:26:49', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"videos\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'videos field', 'videos-field', 'publish', 'closed', 'closed', '', 'group_5de797f0f1015', '', '', '2019-12-11 10:24:18', '2019-12-11 10:24:18', '', 0, 'http://laroujou3.com/?post_type=acf-field-group&#038;p=88', 0, 'acf-field-group', '', 0),
(89, 1, '2019-12-04 11:27:13', '2019-12-04 11:27:13', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'video', 'video', 'publish', 'closed', 'closed', '', 'field_5de797fbe556c', '', '', '2019-12-11 10:24:18', '2019-12-11 10:24:18', '', 88, 'http://laroujou3.com/?post_type=acf-field&#038;p=89', 0, 'acf-field', '', 0),
(99, 1, '2019-12-04 13:00:39', '2019-12-04 13:00:39', '', 'aaaa', '', 'trash', 'closed', 'closed', '', '__trashed', '', '', '2019-12-04 13:00:39', '2019-12-04 13:00:39', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=99', 0, 'documents', '', 0),
(100, 1, '2019-12-04 13:39:36', '2019-12-04 13:39:36', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \r\nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \r\nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \r\nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \r\nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \r\nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \r\n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '1 وثيقة', '', 'publish', 'closed', 'closed', '', 'doc', '', '', '2019-12-10 15:23:45', '2019-12-10 15:23:45', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=100', 0, 'documents', '', 0),
(106, 1, '2019-12-10 13:53:47', '2019-12-10 13:53:47', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور \nأنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد \nأكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس \nأيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت \nنيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا \nكيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.\" \n\"سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '1 وثيقة', '', 'inherit', 'closed', 'closed', '', '100-autosave-v1', '', '', '2019-12-10 13:53:47', '2019-12-10 13:53:47', '', 100, 'http://laroujou3.com/2019/12/10/100-autosave-v1/', 0, 'revision', '', 0),
(107, 1, '2019-12-10 14:05:23', '2019-12-10 14:05:23', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '2 وثيقة', '', 'publish', 'closed', 'closed', '', '2-%d9%88%d8%ab%d9%8a%d9%82%d8%a9', '', '', '2019-12-10 15:23:31', '2019-12-10 15:23:31', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=107', 0, 'documents', '', 0),
(116, 1, '2019-12-10 14:07:57', '2019-12-10 14:07:57', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '5 وثيقة', '', 'publish', 'closed', 'closed', '', '5-%d9%88%d8%ab%d9%8a%d9%82%d8%a9', '', '', '2019-12-10 15:23:11', '2019-12-10 15:23:11', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=116', 0, 'documents', '', 0),
(117, 1, '2019-12-10 14:08:46', '2019-12-10 14:08:46', 'سيت يتبيرسبايكياتيس يوندي أومنيس أستي ناتيس أيررور سيت فوليبتاتيم أكيسأنتييوم \r\nدولاريمكيو لايودانتيوم,توتام ريم أبيرأم,أيكيو أبسا كيواي أب أللو أنفينتوري فيرأتاتيس ايت \r\nكياسي أرشيتيكتو بيتاي فيتاي ديكاتا سيونت أكسبليكابو. نيمو أنيم أبسام فوليوباتاتيم كيواي \r\nفوليوبتاس سايت أسبيرناتشر أيوت أودايت أيوت فيوجايت, سيد كيواي كونسيكيونتشر ماجناي \r\nدولارس أيوس كيواي راتاشن فوليوبتاتيم سيكيواي نيسكايونت. نيكيو بوررو كيوايسكيوم', '6وثيقة', '', 'publish', 'closed', 'closed', '', '6%d9%88%d8%ab%d9%8a%d9%82%d8%a9', '', '', '2019-12-10 15:22:55', '2019-12-10 15:22:55', '', 0, 'http://laroujou3.com/?post_type=documents&#038;p=117', 0, 'documents', '', 0),
(119, 1, '2019-12-10 15:22:06', '2019-12-10 15:22:06', '', 'وثيقة 4', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس أيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت نيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا كيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.', 'inherit', 'open', 'closed', '', '%d9%88%d8%ab%d9%8a%d9%82%d8%a9-4', '', '', '2019-12-10 15:22:51', '2019-12-10 15:22:51', '', 117, 'http://laroujou3.com/wp-content/uploads/2019/12/وثيقة-4.txt', 0, 'attachment', 'text/plain', 0),
(120, 1, '2019-12-10 15:22:06', '2019-12-10 15:22:06', '', 'وثيقة 1', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس أيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت نيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا كيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.', 'inherit', 'open', 'closed', '', '%d9%88%d8%ab%d9%8a%d9%82%d8%a9-1', '', '', '2019-12-10 15:22:48', '2019-12-10 15:22:48', '', 117, 'http://laroujou3.com/wp-content/uploads/2019/12/وثيقة-1.txt', 0, 'attachment', 'text/plain', 0),
(121, 1, '2019-12-10 15:22:08', '2019-12-10 15:22:08', '', 'وثيقة 2', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس أيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت نيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا كيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.', 'inherit', 'open', 'closed', '', '%d9%88%d8%ab%d9%8a%d9%82%d8%a9-2', '', '', '2019-12-10 15:22:46', '2019-12-10 15:22:46', '', 117, 'http://laroujou3.com/wp-content/uploads/2019/12/وثيقة-2.txt', 0, 'attachment', 'text/plain', 0),
(122, 1, '2019-12-10 15:22:08', '2019-12-10 15:22:08', '', 'وثيقة 3', 'لوريم ايبسوم دولار سيت أميت ,كونسيكتيتور أدايبا يسكينج أليايت,سيت دو أيوسمود تيمبور أنكايديديونتيوت لابوري ات دولار ماجنا أليكيوا . يوت انيم أد مينيم فينايم,كيواس نوستريد أكسير سيتاشن يللأمكو لابورأس نيسي يت أليكيوب أكس أيا كوممودو كونسيكيوات . ديواس أيوتي أريري دولار إن ريبريهينديرأيت فوليوبتاتي فيلايت أيسسي كايلليوم دولار أيو فيجايت نيولا باراياتيور. أيكسسيبتيور ساينت أوككايكات كيوبايداتات نون بروايدينت ,سيونت ان كيولبا كيو أوفيسيا ديسيريونتموليت انيم أيدي ايست لابوريوم.', 'inherit', 'open', 'closed', '', '%d9%88%d8%ab%d9%8a%d9%82%d8%a9-3', '', '', '2019-12-10 15:22:43', '2019-12-10 15:22:43', '', 117, 'http://laroujou3.com/wp-content/uploads/2019/12/وثيقة-3.txt', 0, 'attachment', 'text/plain', 0),
(130, 1, '2019-12-11 11:23:00', '2019-12-11 11:23:00', 'la deuxième', 'video2', '', 'publish', 'closed', 'closed', '', 'video2', '', '', '2019-12-23 11:23:42', '2019-12-23 11:23:42', '', 0, 'http://laroujou3.com/?post_type=videos&#038;p=130', 0, 'videos', '', 0),
(134, 1, '2019-12-23 11:17:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-12-23 11:17:11', '0000-00-00 00:00:00', '', 0, 'http://laroujou3.com/?p=134', 0, 'post', '', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'header menu ar', 'header-menu-ar', 0),
(3, 'footer arabic partners menu', 'footer-arabic-partners-menu', 0),
(4, 'footer arabic primary menu', 'footer-arabic-primary-menu', 0),
(5, 'arabic mobile', 'arabic-mobile', 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(37, 2, 0),
(38, 2, 0),
(39, 2, 0),
(40, 2, 0),
(45, 4, 0),
(46, 4, 0),
(47, 4, 0),
(48, 4, 0),
(49, 1, 0),
(56, 3, 0),
(57, 5, 0),
(58, 5, 0),
(59, 5, 0),
(60, 5, 0),
(69, 3, 0),
(70, 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'nav_menu', '', 0, 3),
(4, 4, 'nav_menu', '', 0, 4),
(5, 5, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Structure de la table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'root'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"635681b485d62f79583588633d4a73352445cb0680afddfdf0fca5ff87fb1dfe\";a:4:{s:10:\"expiration\";i:1577272623;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36\";s:5:\"login\";i:1577099823;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '134'),
(18, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(19, 1, 'wp_user-settings-time', '1575466773'),
(20, 1, 'managenav-menuscolumnshidden', 'a:4:{i:0;s:11:\"link-target\";i:1;s:15:\"title-attribute\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:23:\"add-post-type-documents\";i:1;s:12:\"add-post_tag\";}'),
(22, 1, 'nav_menu_recently_edited', '3'),
(23, 1, 'meta-box-order_acf-field-group', 'a:3:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:80:\"acf-field-group-fields,acf-field-group-locations,acf-field-group-options,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(24, 1, 'screen_layout_acf-field-group', '2'),
(25, 1, 'acf_user_settings', 'a:1:{s:15:\"show_field_keys\";s:1:\"0\";}'),
(26, 1, 'closedpostboxes_acf-field-group', 'a:0:{}'),
(27, 1, 'metaboxhidden_acf-field-group', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(28, 1, 'meta-box-order_page', 'a:4:{s:6:\"normal\";s:16:\"gpminvoice-group\";s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:0:\"\";s:8:\"advanced\";s:0:\"\";}'),
(29, 1, 'closedpostboxes_page', 'a:0:{}'),
(30, 1, 'metaboxhidden_page', 'a:0:{}');

-- --------------------------------------------------------

--
-- Structure de la table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'root', '$P$Bf2tiKxWNNSNg5jkYjYK9Bl6R1QNLb.', 'root', 'elmanaahoussem@gmail.com', '', '2019-09-27 13:44:12', '', 0, 'root');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Index pour la table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Index pour la table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Index pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Index pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Index pour la table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Index pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Index pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=390;

--
-- AUTO_INCREMENT pour la table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;

--
-- AUTO_INCREMENT pour la table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT pour la table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
